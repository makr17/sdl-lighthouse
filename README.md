# SDL Lighthouse

Playing with SDL visualization for light patterns that will ultimately
be output to the lights on the house via
[E1.31[(https://en.wikipedia.org/wiki/Architecture_for_Control_Networks#External_extensions).

The physical lights are strips of
[APA102C LEDS](https://components101.com/displays/apa102-rgb-led),
60/meter.
The lighting strips are wired into several
[Pixlite Long Range Isolated Receivers](https://www.advateklights.com/products/shop/pixlite-long-range-isolated-receiver),
which are in turn connected via ethernet cable to a
[Pixlite 16 Long Range Mk2](https://www.advateklights.com/products/shop/pixlite-16-long-range-mk2).
That board is on a network with a
[Raspberry Pi (currently a 3b+)](https://www.raspberrypi.com/products/raspberry-pi-3-model-b-plus/),
which generates lighting patterns and emits E1.31 to the Pixlite,
which drives the Pixlite receivers, which drive the lights strips.

I'm modeling the lights on the house as 2x2 rectangles on a black-filled
SDL canvas.  The lowest two segments exist currently, the rest are
in-progress, hopefully up "soon".  The main intent here is to work
on visualizations before all the lights are installed.

I'm targetting a smooth 60fps refresh rate driven by the rpi.
Part of that is tracking time spent rendering a visualization and
sending it out over the network, so that we only sleep for any time
remaining in our 16.67ms time budget before starting on the next frame.

Primary purpose is coding up visualizations to see how they look in 2-space.
The first attempt is a simple rainbow gradient horizontally-aligned
across all the lights.  Two spans of lights that overlap horizontally
should show the same color at all times as the rainbow shifts slowly
to the right.

I'm modelling color for each LED as
[HSV](https://en.wikipedia.org/wiki/HSL_and_HSV),
primarily because it makes smooth color (hue) transitions dirt simple,
just slowly adjust the Hue angle in the structure.
