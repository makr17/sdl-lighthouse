use palette::{FromColor, Hsv, Srgb};
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::{Point, Rect};
use std::thread::sleep;
use std::time::{Duration, Instant};

struct Stats {
    count: usize,
    iterations: u32,
    timing: Vec<Duration>,
}
impl Stats {
    fn new(count: usize) -> Self {
        Self {
            count,
            iterations: 0,
            timing: vec![],
        }
    }
    fn update(&mut self, elapsed: Duration) {
        self.iterations += 1;
        self.timing.push(elapsed);
    }
    fn print_summary(&mut self) {
        println!("{} lights over {} frames", self.count, self.iterations);
        let total_time: Duration = self.timing.iter().sum();
        self.timing.sort();
        println!("min  frame time: {:?}", self.timing.first().unwrap());
        println!("mean frame time: {:?}", total_time/self.iterations);
        println!(
            "p90  frame time: {:?}",
            self.timing[(self.iterations as f32 * 0.9) as usize]
        );
        println!("max  frame time: {:?}", self.timing.last().unwrap());

    }
}

#[derive(Clone, Copy, Debug)]
struct Light {
    point: Point,
    color: Hsv,
}

#[derive(Debug)]
struct Segment {
    start: Point,
    end: Point,
    lights: Vec<Light>,
}
impl Segment {
    fn new(start: Point, end: Point, count: i32) -> Self {
        let rise = end.y() - start.y();
        let rise_step = rise as f32 / (count as f32 - 1.0);
        //println!("rise step={rise_step}");
        let run = end.x() - start.x();
        let run_step = run as f32/ (count as f32 - 1.0);
        //println!("run step={run_step}");
        let black = Hsv::new(0.0, 0.0, 0.0);
        let lights = (0..count).into_iter()
            .map(|i| Light {
                point: Point::new(
                    start.x() + (i as f32 * run_step) as i32,
                    start.y() + (i as f32 * rise_step) as i32,
                ),
                color: black
            })
            .collect();
        Self {
            start,
            end,
            lights,
        }
    }
}
#[test]
fn test_segment_create() {
    let segment = Segment::new(Point::new(0,0), Point::new(100, 100), 11);
    assert_eq!(segment.lights.len(), 11);
    assert_eq!(segment.lights[0].point, Point::new(0,0));
    assert_eq!(segment.lights[5].point, Point::new(50,50));
    assert_eq!(segment.lights[10].point, Point::new(100,100));
    let neg = Segment::new(Point::new(0,100), Point::new(100, 0), 11);
    assert_eq!(neg.lights[5].point, Point::new(50,50));
}

pub fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let rh = 3;
    let rw = 3;

    // build segments with spatial data
    let segments = vec![
        // porch left, 4.1m
        Segment::new(Point::new(38, 912), Point::new(810, 701), 224),
        // porch right, 4.1m
        Segment::new(Point::new(867, 701), Point::new(1628, 935), 239),
        // upper left, 6.0m
        Segment::new(Point::new(48, 417), Point::new(1168, 64), 360),
        // upper right, 2.4m
        Segment::new(Point::new(1230, 64), Point::new(1635, 207), 144),
        // bay left, 3.3m
        Segment::new(Point::new(1150, 436), Point::new(1747, 235), 198),
        // bay left, 3.0m
        Segment::new(Point::new(1801, 235), Point::new(2259, 415), 180)
    ];
    // calculate our x/y bounds
    // used to size the window
    //         create the gradient
    //         calculate light color starting value
    let mut ys = segments.iter()
        .flat_map(|s| [s.start.y, s.end.y])
        .collect::<Vec<i32>>();
    // sort reverse
    ys.sort_by(|a, b| b.cmp(a));
    let h_max = *ys.first().unwrap();
    let h = h_max as u32 + 30;
    let h_min = *ys.last().unwrap();
    #[allow(unused_variables)]
    let dh = h_max - h_min;
    let mut xs = segments.iter()
        .flat_map(|s| [s.start.x, s.end.x])
        .collect::<Vec<i32>>();
    // sort reverse
    xs.sort_by(|a, b| b.cmp(a));
    let w_max = *xs.first().unwrap();
    let w = w_max as u32 + 30;
    let w_min = *xs.last().unwrap();
    #[allow(unused_variables)]
    let dw = w_max - w_min;

    // size window based on x/y of lights
    let window = video_subsystem.window("lighthouse visualization", w, h)
        .position_centered()
        .build()
        .unwrap();
    let mut canvas = window.into_canvas().build().unwrap();

    // now that everything is laid out, build a vec of lights
    // probably a good idea to keep this is E1.31 output order
    let mut lights: Vec<Light> = vec![];
    for segment in segments.iter() {
        lights.append(&mut segment.lights.clone());
    }
    let mut stats = Stats::new(lights.len());
    // saturation and brightness levels
    let sat = 1.0; //0.75;
    let val = 1.0; //0.5;

    // horizontal shifting gradient
    let step = 360.0/dw as f32;
    // render gradient onto lights
    // pick starting H angle based on x position of the light
    for l in lights.iter_mut() {
        // pick start color from light x position
        // normalize by w_min
        l.color = Hsv::new((l.point.x - w_min) as f32 * step, sat, val);
    }

    // vertical shifting gradient
    // doesn't work as well visually
    // mainly due to the gap between upper and lower eaves
    /*
    let mut step = 360.0/dh as f32;
    for l in lights.iter_mut() {
        l.color = Hsv::new((l.point.y - h_min) as f32 * step, sat, val);
    }
     */

    canvas.set_draw_color(Color::RGB(0, 0, 0));
    canvas.clear();
    canvas.present();
    let mut event_pump = sdl_context.event_pump().unwrap();
    let target_framerate = 60;
    let target_frame_duration = Duration::new(0, 1_000_000_000/target_framerate);
    'running: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'running
                },
                Event::KeyDown { keycode: Some(Keycode::Space), .. } => {
                    stats.print_summary();
                },
                _ => {}
            }
        }
        let now = Instant::now();
        for light in lights.iter_mut() {
            let rgb = Srgb::from_color(light.color);
            canvas.set_draw_color(
                Color::RGB(
                    (rgb.red * 255.0) as u8,
                    (rgb.green * 255.0) as u8,
                    (rgb.blue * 255.0) as u8
                )
            );
            let _r = canvas.fill_rect(Rect::from_center(light.point, rw, rh));
            light.color.hue -= step;
        }

        canvas.present();

        //println!("{:?}", now.elapsed());
        // sleep until it is time to start the next frame
        // targeting a smooth framerate
        stats.update(now.elapsed());
        if now.elapsed() < target_frame_duration {
            sleep(target_frame_duration - now.elapsed());
        }
    }

    stats.print_summary();
}
